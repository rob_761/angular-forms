import {Component, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormControl, FormGroup, NgForm} from "@angular/forms";
import {Validators} from "@angular/forms";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit{
  genders = ['male', 'female'];
  signupForm: FormGroup;
  forbiddenUsernames = ['Chris', 'Anna'];

  ngOnInit() {
    this.signupForm = new FormGroup({ //initialise form before rendering template
      'userData': new FormGroup({
        'username': new FormControl(
          null, [
            Validators.required, this.forbiddenNames.bind(this)]), //pass reference to validator method with binding for angular
        'email': new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails.bind(this)) //array of Validators
      }),
        'gender': new FormControl('male'),
      'hobbies': new FormArray([]) //empty at beginning
    });
    this.signupForm.valueChanges.subscribe(
      (value) => {
        console.log(value);
      }
    );

    this.signupForm.statusChanges.subscribe(
      (value) => {
        console.log(value);
      }
    );

    this.signupForm.setValue({
      'userData': {
        'username': 'Max',
        'email': 'max@test.com'
      },
      'gender': 'male',
      'hobbies': []
    });

    this.signupForm.patchValue({
      'userData': {
        'username': 'Anna',
        'email': 'anna@test.com'
      }
    });
  }

  onSubmit() {
    console.log(this.signupForm);
    this.signupForm.reset();
  }

  onAddHobby() {
    const control = new FormControl(null, Validators.required);
    (<FormArray>this.signupForm.get('hobbies')).push(control) //must cast
  }

  forbiddenNames(control: FormControl): {[s: string]: boolean} {//key value pair of string and boolean returned by function
  if (this.forbiddenUsernames.indexOf(control.value) !== -1) { //checking if value exists in array
    return {'nameIsForbidden': true}; //object with error code and true
  }
  return null; //must return nothing or null if false
  }

  //asynchronouse validator
  forbiddenEmails(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'test@test.com') {
          resolve({'emailIsForbidden': true}); //resolve because promise
        } else {
          resolve(null);
        }
      }, 1500);
      }
    );
    return promise;
  }
}
