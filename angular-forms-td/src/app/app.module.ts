import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ReactiveComponent } from './reactive/reactive.component';

import {RouterModule, Routes} from "@angular/router";
import { TemplateComponent } from './template/template.component';

const appRoutes: Routes = [
  {path: 'reactive', component: ReactiveComponent},
  {path: 'template', component: TemplateComponent}

]

@NgModule({
  declarations: [
    AppComponent,
    ReactiveComponent,
    TemplateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
